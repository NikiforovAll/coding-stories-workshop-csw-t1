namespace ASPNETCore.HowTos.OpenAPI
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.ApiExplorer;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.PlatformAbstractions;
    using Microsoft.OpenApi.Models;

    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.DefaultApiVersion = new ApiVersion(2, 0);
                options.AssumeDefaultVersionWhenUnspecified = true;
            });
            services.AddSwaggerGen(options =>
            {
                var title = "Sample API";
                var description = "A sample application with Swagger, Swashbuckle, and API versioning.";
                var contact = new OpenApiContact() { Name = "Sample User", Email = "sample@user.com" };
                var license = new OpenApiLicense() { Name = "MIT", Url = new Uri("https://opensource.org/licenses/MIT") };

                options.SwaggerDoc("v1", new OpenApiInfo()
                {
                    Title = title,
                    Version = "v1",
                    Description = description,
                    Contact = contact,
                    License = license
                });
                options.SwaggerDoc("v2", new OpenApiInfo()
                {
                    Title = "Sample API",
                    Version = "v2",
                    Description = "A sample application with Swagger, Swashbuckle, and API versioning.",
                    Contact = contact,
                    License = license
                });
                options.OperationFilter<SwaggerDefaultValues>();

                // integrate xml comments
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var fileName = typeof(Startup).GetType().Assembly.GetName().Name + ".xml";
                var commentsPath = Path.Combine(basePath, fileName).ToString();
                options.IncludeXmlComments(XmlCommentsFilePath);
            });
            services.AddVersionedApiExplorer(options => options.GroupNameFormat = "'v'VVV");
        }

        public void Configure(
            IApplicationBuilder app, IWebHostEnvironment env, IApiVersionDescriptionProvider apiVersionDescriptionProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints => endpoints.MapControllers());
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                foreach (var description in apiVersionDescriptionProvider.ApiVersionDescriptions.Reverse())
                {
                    c.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json",
                        description.GroupName.ToUpperInvariant());
                }
                c.RoutePrefix = string.Empty;
            });
        }

        public static string XmlCommentsFilePath
        {
            get
            {
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var fileName = typeof(Startup).GetTypeInfo().Assembly.GetName().Name + ".xml";
                return Path.Combine(basePath, fileName);
            }
        }
    }
}
